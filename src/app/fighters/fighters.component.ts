import { Component, OnInit } from '@angular/core';
import {FightersService} from '../Services/fighters.service';

@Component({
  selector: 'app-fighters',
  templateUrl: './fighters.component.html',
  styleUrls: ['./fighters.component.css']
})
export class FightersComponent implements OnInit {

  constructor(private fightersService: FightersService) {}
  fighters: Array<any>;
  games: Array<any>;
  game: any;

  ngOnInit() {
    this.fightersService.getAllFighters().subscribe(fighter => {
      this.fighters = fighter;
      this.adjustImageURL();
    });

    this.fightersService.getAllGames().subscribe( game => {this.games = game; });
  }

  public onChanged(gameName): void {
    this.fighters = new Array<any>();
    console.log(this.game);

    // if (this.game === 'All') {
    //   this.ngOnInit();
    //   return;
    // }

    this.fightersService.getFightersByGameType(gameName).subscribe(gameType => {
      console.log(gameType);
      this.fighters = gameType;
      this.adjustImageURL();
    });
  }

  public adjustImageURL(): void {
    for (let i = 0; i < this.fighters.length; i++) {
      // console.log((Object.values(this.fighters)[i].image as string).split(',').length);
      const tempArray = (Object.values(this.fighters)[i].image as string).split(',');
      for (let j = 0; j < (Object.values(this.fighters)[i].image as string).split(',').length - 1; j++) {
        tempArray.shift();
      }
      (Object.values(this.fighters)[i].image as string) = tempArray[0];
    }
  }

  public setSymbolColor(id, value) {
    console.log(id + ' ' + value);
    console.log(this.fighters[id]);
    const svg = document.getElementById((id).toString().concat('fighter'));
    if (value) {
      // console.log(this.fighters[id - 1]);
      svg.setAttribute('fill', this.fighters[id].game.gameColor);
      svg.setAttribute('fill-opacity', '1');
    } else {
      svg.setAttribute('fill', 'gray');
      svg.setAttribute('fill-opacity', '.5');
    }
  }

  public setFilterBarSvgColor(id, value) {
    console.log(id + ' ' + value);
    console.log(this.games[id]);
    const svg = document.getElementById((id - 1).toString().concat('game'));
    if (value) {
      // console.log(this.games);
      svg.setAttribute('fill', this.games[id - 1].gameColor);
      svg.setAttribute('fill-opacity', '1');
    } else {
      svg.setAttribute('fill', 'gray');
      svg.setAttribute('fill-opacity', '.5');
    }
  }
}

window.onscroll = () => {myFunction(); };

let navBar: any;
let sticky: any;


function myFunction() {
   navBar = document.getElementById('navbar');
   sticky = 351;
   if (window.pageYOffset > sticky) {
     navBar.classList.add('sticky');
   } else {
     navBar.classList.remove('sticky');
   }
 }
